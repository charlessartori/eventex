#coding: utf-8

__author__ = 'charles'

from django.contrib import admin
from .models import Speaker, Contact
from .models import Talk, Media, Course


class ContactInline(admin.TabularInline):
    model = Contact
    extra = 1

class MediaInline(admin.TabularInline):
    model = Media
    extra = 1

class SpeakerAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug', 'url', 'description','avatar')
    search_fields = ('name','slug','url','description')
    inlines = [ContactInline,]
    prepopulated_fields = {'slug': ('name',)}

class TalkAdmin(admin.ModelAdmin):
    list_display = ('title','description','start_time')
    inlines = [MediaInline,]

class CourseAdmin(admin.ModelAdmin):
    list_display = ('title','description','start_time','slots')

admin.site.register(Speaker, SpeakerAdmin)
admin.site.register(Talk, TalkAdmin)
admin.site.register(Course, CourseAdmin)

