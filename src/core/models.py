#coding: utf-8
from django.db import models
from django.utils.translation import ugettext_lazy as _
from datetime import time
########################################################################################
######### Model Speaker ################################################################

class Speaker(models.Model):
    name = models.CharField(_(u'Nome'), max_length=255)
    slug = models.SlugField(_(u'Slug'))
    url = models.URLField(_(u'Url'))
    description = models.TextField(_(u'Descrição'), blank=True)
    avatar = models.FileField(_(u'Avatar'),upload_to=u'palestrantes',blank=True,null=True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u"Palestrante"
        verbose_name_plural = u"Palestrantes"

######### End Model Speaker #####################################################################
#################################################################################################

########################################################################################
######### Model Contact ################################################################



class KindContactManager(models.Manager):
    def __init__(self, kind):
        super(KindContactManager, self).__init__()
        self.kind = kind

    def get_query_set(self):
        qs = super(KindContactManager, self).get_query_set()
        qs = qs.filter(kind=self.kind)
        return qs

class Contact(models.Model):
    KINDS = (
        ('P', _('Telefone')),
        ('E', _('E-mail')),
        ('F', _('Fax')),
    )

    speaker = models.ForeignKey('Speaker', verbose_name=_(u'palestrante'))
    kind = models.CharField(_(u'Tipo'), max_length=1,choices=KINDS)
    value = models.CharField(_(u'Valor'), max_length=255)

    objects = models.Manager()
    phones = KindContactManager('P')
    email = KindContactManager('E')
    faxes = KindContactManager('F')

######### End Model Contact #####################################################################
#################################################################################################

########################################################################################
######### Model Talk ###################################################################

class PeriodManager(models.Manager):
    midday = time(12)

    def at_morning(self):
        qs = self.filter(start_time__lt=self.midday)
        qs = qs.order_by('start_time')
        return qs

    def at_afternoon(self):
        qs = self.filter(start_time__gte=self.midday)
        qs = qs.order_by('start_time')
        return qs


class Talk(models.Model):
    title = models.CharField(u'Título',max_length=200)
    description = models.TextField(u'Descrição')
    start_time = models.TimeField(u'Hora de início',blank=True)
    speakers = models.ManyToManyField('Speaker', verbose_name=_(u'palestrante'))

    objects = PeriodManager()

    @property
    def slides(self):
        return self.media_set.filter(type='SL')

    @property
    def videos(self):
        return self.media_set.filter(type='YT')

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u"Palestra"
        verbose_name_plural = u"Palestras"

######### End Model Talk ########################################################################
#################################################################################################

########################################################################################
######### Model Course ###################################################################

class Course(Talk):

    slots = models.IntegerField(u'Vagas')
    notes = models.TextField(u'Notas importantes')

    objects = PeriodManager()

    class Meta:
        verbose_name = u"Curso"
        verbose_name_plural = u"Cursos"
######### End Model Course ########################################################################
#################################################################################################

########################################################################################
######### Model Media ###################################################################

class Media(models.Model):
    MEDIAS = (
        ('SL','SlideShare'),
        ('YT', 'Youtube'),
    )
    talk = models.ForeignKey('Talk')
    type = models.CharField(u'Tipo',max_length=2, choices=MEDIAS)
    title = models.CharField(u'Título', max_length=255)
    media_id = models.CharField(u'ID mídia',max_length=255)

    def __unicode__(self):
        return u'%s - %s' % (self.talk.title, self.title)

######### End Model Media ########################################################################
#################################################################################################
