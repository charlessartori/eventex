#coding: utf-8
from django.test import TestCase
from .models import Speaker
from django.core.urlresolvers import reverse
from .models import Contact
from .models import Talk
from .models import PeriodManager
from .models import Media

########################################################################################
######### Tests Speaker ################################################################

class SpeakerModelTest(TestCase):
    def setUp(self):
        self.speaker = Speaker(
            name="Charles Sartori",
            slug="charles-sartori",
            url="http://google.com",
            description="the best",
            avatar=""
        )
        self.speaker.save()

    def test_create(self):
        self.assertEqual(1,self.speaker.pk)

    def test_unicode(self):
        self.assertEqual(u"Charles Sartori", unicode(self.speaker))


class SpeakerDetailTest(TestCase):
    def setUp(self):
        Speaker.objects.create(
            name="Charles Sartori",
            slug="charles-sartori",
            url="http://google.com",
            description="the best",
            avatar=""
        )
        self.response = self.client.get(reverse('core:speaker_detail',
                                                kwargs={'slug': 'charles-sartori'}))

    def test_get(self):
        self.assertEqual(200,self.response.status_code)

    def test_template(self):
        self.assertTemplateUsed(self.response,'core/speaker_detail.html')

    def test_speaker_in_context(self):
        speaker = self.response.context['speaker']
        self.assertIsInstance(speaker,Speaker)


########################################################################################
######### Test Contact #################################################################

class ContactModelTest(TestCase):
    def setUp(self):
        self.speaker = Speaker.objects.create(
            name="Charles Sartori",
            slug="charles-sartori",
            url="http://google.com",
            description="the best",
            avatar=""
        )

    def test_create_email(self):
        contact = Contact.objects.create(speaker=self.speaker, kind='E',
                                        value='charles.sartori@gmail.com')
        self.assertEqual(1,contact.pk)

    def test_create_phone(self):
        contact = Contact.objects.create(speaker=self.speaker, kind='P',
                                        value='55-33149999')
        self.assertEqual(1,contact.pk)

    def test_create_fax(self):
        contact = Contact.objects.create(speaker=self.speaker, kind='F',
                                        value='55-33148888')
        self.assertEqual(1,contact.pk)

########################################################################################
######### Test Talk ####################################################################

class TalkModelTest(TestCase):
    def setUp(self):
        self.talk = Talk.objects.create(
            title=u'Porque Python?',
            description=u'porque Python é legal!',
            start_time='10:00'
        )

    def test_create(self):
        self.assertEqual(1,self.talk.pk)

    def test_unicode(self):
        self.assertEqual(u'Porque Python?', unicode(self.talk))

    def test_period_manager(self):
        self.assertIsInstance(Talk.objects, PeriodManager)


class TalksViewTest(TestCase):
    def setUp(self):
        self.response = self.client.get(reverse('core:talks'))

    def test_get(self):
        self.assertEqual(200, self.response.status_code)

    def test_template(self):
        self.assertTemplateUsed(self.response,'core/talks.html')

    def test_morning_talks_in_context(self):
        self.assertIn('morning_talks',self.response.context)

    def test_afternoon_talks_in_context(self):
        self.assertIn('afternoon_talks', self.response.context)

class PeriodManagerTest(TestCase):
    def setUp(self):
        Talk.objects.create(title=u"Morning Talk", start_time='10:00')
        Talk.objects.create(title=u"Afternoon Talk", start_time='12:00')

    def test_morning(self):
        self.assertQuerysetEqual(
            Talk.objects.at_morning(), ['Morning Talk'],
            lambda t: t.title
        )

    def test_afternoon(self):
        self.assertQuerysetEqual(
            Talk.objects.at_afternoon(), ['Afternoon Talk'],
            lambda t: t.title
        )

class TalkDetailTest(TestCase):
    def setUp(self):
        Talk.objects.create(
            title='Talk',
            start_time='10:00'
        )
        self.response = self.client.get(reverse('core:talk_detail',args=[1]))

    def test_get(self):
        self.assertEqual(200,self.response.status_code)

    def test_template(self):
        self.assertTemplateUsed(self.response,'core/talk_detail.html')

    def test_talk_in_context(self):
        talk = self.response.context['talk']
        self.assertIsInstance(talk,Talk)

########################################################################################
######### Test Media ####################################################################

class MediaModelTest(TestCase):
    def setUp(self):
        talk = Talk.objects.create(
            title =u'Talk 1',
            start_time='10:00'
        )
        self.media = Media.objects.create(
            talk=talk,
            type='YT',
            media_id='QjA5faZF1A8',
            title='Video'
        )

    def test_create(self):
        self.assertEqual(1,self.media.pk)

    def test_unicode(self):
        self.assertEqual("Talk 1 - Video", unicode(self.media))
