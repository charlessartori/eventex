#coding: utf-8

__author__ = 'charles'

from django.conf.urls import patterns, include, url

urlpatterns = patterns('src.core.views',
    url(r'^palestrante/(?P<slug>[\w-]+)/$', 'speaker_detail',
        name='speaker_detail'),
    url(r'^palestrantes/$', 'speakers_detail',
        name='speakers_detail'),
    url(r'^palestras/$','talks', name='talks'),
    url(r'^palestras/(\d+)/$', 'talk_detail', name='talk_detail'),
)
