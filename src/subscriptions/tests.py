#coding: utf-8


__author__ = 'charles'

from django.test import TestCase
from django.core import mail
from django.core.urlresolvers import reverse
from django.db import IntegrityError
from .models import Subscription
from .forms import SubscriptionForm
from mock import Mock
from .admin import SubscriptionAdmin, Subscription, admin
from django.contrib.auth.models import User

########################################################################################
######### Tests Subscription URL #######################################################

class SubscriptionUrlTest(TestCase):
    def test_get_subscribe_page(self):
        "TESTE: Verifica rota para .../inscricao"
        response = self.client.get('/inscricao/')
        self.assertEquals(200, response.status_code)

    def test_get_subscribe_page(self):
        "TESTE: Tenta acessar .../inscricao/"
        response = self.client.get(reverse('subscriptions:subscribe'))
        self.assertEquals(200, response.status_code)


########################################################################################
######### Tests Subscription VIEW ######################################################

class SubscribeViewTest(TestCase):
    def setUp(self):
        self.response = self.client.get(reverse('subscriptions:subscribe'))

    def test_get(self):
        "TESTE: Ao visitar /inscricao/ a página de inscrição é exibida."
        self.assertEquals(200, self.response.status_code)

    def test_use_template(self):
        "TESTE: O corpo da resposta deve conter a renderização de um template"
        self.assertTemplateUsed(self.response, 'subscriptions/subscription_form.html')

    def test_has_form(self):
        "A resposta deve conter o formulário de inscrição"
        self.assertIsInstance(self.response.context['form'], SubscriptionForm)

    def test_form_has_fields(self):
        "O formulário deve conter campos: name, email, cpf e phone."
        form = self.response.context['form']
        self.assertItemsEqual(['name','email','cpf','phone'], form.fields)

    def test_html(self):
        "o html deve conter os campos do formulário"
        self.assertContains(self.response, '<form')
        self.assertContains(self.response, '<input', 8)
        self.assertContains(self.response, 'type="text"', 5)
        self.assertContains(self.response, 'type="submit"')

class SubscribeViewPostTest(TestCase):
    def setUp(self):
        data = dict(name='Charles Sartori', cpf='00000000000',
            email='charles.sartori@gmail.com', phone='55-33159999')
        self.response = self.client.post(reverse('subscriptions:subscribe'), data)

    def test_redirects(self):
        "TESTE: Post deve redirecionar para página de sucesso."
        self.assertRedirects(self.response, reverse('subscriptions:success', args=[1]))

    def test_save(self):
        "TESTE: Post deve salvar Subscription no banco."
        self.assertTrue(Subscription.objects.exists())
    def test_email_sent(self):
        "TESTE: Post deve notificar visitante por email"
        self.assertEquals(1, len(mail.outbox))


class SubscribeViewInvalidPostTest(TestCase):
    def setUp(self):
        data = dict(name='Charles Sartori', cpf='000000000001',
            email='charles.sartori@gmail.com', phone='55-33159999')
        self.response = self.client.post(reverse('subscriptions:subscribe'), data)

    def test_show_page(self):
        "TESTE: Post inválido não deve redirecionar."
        self.assertEqual(200, self.response.status_code)

    def test_form_errors(self):
        "TESTE: Form deve conter erros."
        self.assertTrue(self.response.context['form'].errors)

    def test_must_not_save(self):
        "TESTE: Dados não devem ser salvos."
        self.assertFalse(Subscription.objects.exists())


class SuccessViewTest(TestCase):
    def setUp(self):
        s = Subscription.objects.create(name='Charles Sartori', cpf='00000000000',
            email='charles.sartori@gmail.com', phone='55-33149999')
        self.resp = self.client.get(reverse('subscriptions:success', args=[s.pk]))
    def test_get(self):
        "Visita /inscrica/1/ e retorna 200."
        self.assertEquals(200, self.resp.status_code)
    def test_template(self):
        "Renderiza o template"
        self.assertTemplateUsed(self.resp, 'subscriptions/subscription_detail.html')
    def test_context(self):
        "Verifica instância de subscription no contexto."
        subscription = self.resp.context['subscription']
        self.assertIsInstance(subscription, Subscription)
    def test_html(self):
        "Página deve conter nome do cadastrado."
        self.assertContains(self.resp, 'Charles Sartori')

class SuccessViewNotFound(TestCase):
    def test_not_found(self):
        "Acesso à inscrição não cadastrada deve retornar 404."
        response = self.client.get(reverse('subscriptions:success', args=[0]))
        self.assertEqual(404, response.status_code)


class CustomActionTest(TestCase):
    def setUp(self):
        Subscription.objects.create(name='Charles Sartori', cpf='012345678901',
            email='charles.sartori@gmail.com', phone='55-33149999')

        self.modeladmin = SubscriptionAdmin(Subscription, admin.site)
        #action!
        self.modeladmin.mark_as_paid(Mock(), Subscription.objects.all())

    def test_update(self):
        'TESTE: Dados devem ser atualizados como pago de acordo com o queryset'
        self.assertEqual(1, Subscription.objects.filter(paid=True).count())

class ExportSubscriptionViewTest(TestCase):
    def setUp(self):
        User.objects.create_superuser('admin', 'admin@admin.com', 'admin')
        assert self.client.login(username='admin', password='admin')
        self.resp = self.client.get(reverse('admin:export_subscriptions'))

    def test_get(self):
        'TESTE: Sucesso ao acessar url de download do arquivo csv.'
        self.assertEqual(200, self.resp.status_code)

    def test_attachment(self):
        'TESTE: Header indicando ao browser que a resposta é um arquivo a ser salvo'
        self.assertTrue('attachment;' in self.resp['Content-Disposition'])


class ExportSubscriptionsNotFound(TestCase):
    def test_404(self):
        "TESTE: login é exigido para download do csv"
        #Quando o usuário não está autentucado, o admin retorna 200 e rendereiza o html de login
        response = self.client.get(reverse('admin:export_subscriptions'))
        self.assertTemplateUsed(response,'admin/login.html')

########################################################################################
######### Tests Subscription MODEL #####################################################

class SubscriptionTest(TestCase):
    #1) O modelo deve ter os campo: name, cpf, email, phone, created_at
    #2) O cpf/email devem ser únicos
    def test_create(self):
        "TESTE: O modelo deve ter nome, cpf, email, phone, created_at"
        s = Subscription.objects.create(
            name='Charles Sartori',
            cpf='012345678901',
            email='charles.sartori@gmail.com',
            phone='55-33149999'
        )
        self.assertEquals(s.id, 1)


class SubscriptionModuleUniqueTest(TestCase):
    def setUp(self):
        #Cria uma primeira inscrição no banco
        Subscription.objects.create(name='Charles Sartori',
            cpf='01234567890',
            email='charles.sartori@gmail.com',
            phone='55-33129999')

    def test_cpt_must_be_unique(self):
        "TESTE: CPF deve ser único"
        #Instancia a inscrição com CPF existente
        s = Subscription(name='Charles Sartori',
            cpf='01234567890',
            email='outro@gmail.com',
            phone='55-33129999')
        #verifica se ocorre o erro de integridade ao persistir
        self.assertRaises(IntegrityError, s.save)



########################################################################################
######### Tests Subscription FORM #####################################################


class SubscriptionFormTest(TestCase) :

    def test_cpf_has_only_digits(self):
        u'CPF deve ter apenas dígitos.'
        form = self.make_and_validate_form(cpf='ABCDE000000')
        self.assertDictEqual(form.errors,
                {'cpf': [u'O CPF deve conter apenas números']})
    def test_cpf_has_11_digits(self):
        u'CPF deve ter exatamente 11 dígitos.'
        form = self.make_and_validate_form(cpf='000000000012')
        self.assertDictEqual(form.errors,
                {'cpf': [u'O CPF deve ter 11 dígitos']})

    def test_must_inform_email_or_phone(self):
        'Email e Phone são opcionais, mas ao menos 1 precisa ser informado.'
        form = self.make_and_validate_form(email='', phone='')
        self.assertEqual(form.errors,
                        {'__all__':[u'Informe seu email ou telefone.']})


    def make_and_validate_form(self, **kwargs):
        data = dict(name='Charles Sartori', email='charles.sartori@gmail.com',
                    cpf='00000000000', phone='55-33149999')
        data.update(kwargs)
        form = SubscriptionForm(data)
        form.is_valid()
        return form





