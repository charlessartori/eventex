# coding: utf-8
from django.core.validators import EMPTY_VALUES

__author__ = 'charles'

from django import forms
from .models import Subscription
from django.utils.translation import ugettext as _
from django.core.exceptions import ValidationError


def CpfValidator(value):
    if not value.isdigit():
        raise ValidationError(_(u'O CPF deve conter apenas números'))
    if len(value) != 11:
        raise ValidationError(_(u'O CPF deve ter 11 dígitos'))




class PhoneWidget(forms.MultiWidget):
    def __init__(self, ddd_length=2, number_length=8, attrs=None):
        widgets = (
            forms.TextInput(attrs={'size': ddd_length}),
            forms.TextInput(attrs={'size': number_length})
        )
        super(PhoneWidget,self).__init__(widgets,attrs)

    def decompress(self, value):
        if not value:
            return[None,None]
        return value.split('-')

class PhoneField(forms.MultiValueField):
    widget = PhoneWidget

    def __init__(self, *args, **kwargs):
        fields = (
            forms.IntegerField(),
            forms.IntegerField()
        )
        super(PhoneField, self).__init__(fields, *args, **kwargs)

    def compress(self, data_list):
        if not data_list:
            return ''
        if data_list[0] in EMPTY_VALUES:
            raise forms.ValidationError(u'DDD inválido')
        if data_list[1] in EMPTY_VALUES:
            raise forms.ValidationError(u'Número inválido')
        return '%s-%s' % tuple(data_list)

def ddd_phone_validator(value):

    var = value.split('-')

    if len(var[0]) != 2:
        raise ValidationError(_(u'O DDD deve ter 2 digitos'))
    if len(var[1]) != 8:
        raise ValidationError(_(u'O telefone deve ter 8 digitos'))




class SubscriptionForm(forms.ModelForm):

    cpf = forms.CharField(label=_('CPF'), validators=[CpfValidator])
    phone = PhoneField(label=_('Telefone'), required=False, validators=[ddd_phone_validator])

    class Meta:
        model = Subscription
        exclude = ('paid',)

    def clean(self):
        super(SubscriptionForm, self).clean()

        if not self.cleaned_data['email'] and \
           not self.cleaned_data['phone']:
            raise forms.ValidationError(_(u'Informe seu email ou telefone.'))

        return self.cleaned_data